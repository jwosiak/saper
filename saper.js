miny = new Array(16);
for (var j=0; j<16; j++){   //postawienie 40 min na planszy
  miny[j] = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
}
for (var j=0; j<40; j++){             //wylosowanie 40 pol z minami
  x = Math.floor(Math.random() * 16);
  y = Math.floor(Math.random() * 16);
  if (miny[x][y] == -1){
    j--;
  }
  else{
    miny[x][y] = -1;
  }
}

var ramka = document.getElementById("ramka");
kwadraty = new Array(16);
for (var j=0; j<16; j++){     //wygenerowanie pol na planszy
  kwadraty[j] = new Array(16);
  for (var i=0; i<16; i++){
    kwadraty[j][i] = document.createElement("div");
    kwadraty[j][i].setAttribute("onmouseup","lewy(event)");
    kwadraty[j][i].setAttribute("oncontextmenu","prawy(event)");
    kwadraty[j][i].style.left = (j*37).toString() + "px";
    kwadraty[j][i].style.top = (-j*592).toString() + "px";

    wsp = j.toString();
    if (j < 10)
      wsp += " ";
    wsp += i.toString();
    if (i < 10)
      wsp += " ";
    if (miny[j][i] == -1){                  //id = "liczbamin|wsp col|wsp row"
      kwadraty[j][i].setAttribute("id",parseInt(9)+wsp);
    }
    else{
      id = parseInt(liczbaMin(miny,j,i))+wsp;
      kwadraty[j][i].setAttribute("id",id);
    }
    kwadraty[j][i].setAttribute("class","kwadracik");
    ramka.appendChild(kwadraty[j][i]);
  }
}

var lklik;  //to bedzie timeout wywolany w funkcji lewy()
koniec = false; //kiedy gra sie skonczy, to koniec = true
var zostalomin = 40;
var start = false; //jesli true, to znaczy ze gra sie juz zaczela
var t0; //czas w momencie startu gry

function lewy(e) {  //odslania pole
  if (koniec)
    return;
  if (!start){
    start = true;
    t0 = new Date();
    startTime();
  }
  var targ;
  if (!e) {
      var e = window.event;
  }
  if (e.target) {
      targ=e.target;
  } else if (e.srcElement) {
      targ=e.srcElement;
  }
  id = targ.getAttribute("id"); //id = "liczbamin|wsp col|wsp row"
  y = parseInt(id.substring(3,5));
  x = parseInt(id.substring(1,3))
  lklik = setTimeout(odslon, 10,x,y);
  //Poniewaz event onmouseup nastepuje zawsze przed oncontextmenu, zatem
  //aby odroznic klikniecie prawego przycisku, od lewego, nalezy opoznic
  //dzialanie lewego, aby zobaczyc czy prawy nie zostal klikniety. Jesli
  //tak, to funkcja prawy() anuluje dzialanie funkcji lewy().
}

var odsPola = 0;

function odslon(x,y){   //odslania pole; jesli pole jest puste (nie ma na nim miny, ani liczby), to odslaniane sa sasiednie pola
  ilemin = parseInt(kwadraty[x][y].getAttribute("id").substring(0,1));
  if (kwadraty[x][y].getAttribute("class") == "kwadracik"){
    kwadraty[x][y].setAttribute("class", ("kwadracik e"+ilemin.toString()));
    odsPola++;
    sprawdzWarunki(); //sprawdzenie czy gra juz sie nie skonczyla
    if (ilemin == 9){  //trafilo na mine
      koniec = true;
      var node = document.createElement("DIV");
      var textnode = document.createTextNode("Koniec gry!");
      node.appendChild(textnode);
      document.getElementById("main").appendChild(node);
    }
    if (ilemin==0){   //wywolanie odslon dla wszystkich sasiadow
      var i,maxi,maxj,j;
      if (x>0)
        i = x-1;
      else
        i = x;
      if (x<15)
        maxi = x+1;
      else
        maxi = x;
      if (y<15)
        maxj = y+1;
      else
        maxj = y;

      for (; i<= maxi; i++){
        if (y>0)
          j = y-1;
        else
          j = y;
        for (; j<= maxj; j++){
          if (i != x || j != y){
            odslon(i,j);
          }
        }
      }
    }
  }
}

function prawy(e){  //powoduje umieszczenie flagi na polu
  if (koniec)
    return;
  if (!start){
    start = true;
    t0 = new Date();
    startTime();
  }
  start = true;
  var targ;
  if (!e) {
      var e = window.event;
  }
  if (e.target) {
      targ=e.target;
  } else if (e.srcElement) {
      targ=e.srcElement;
  }

  id = targ.getAttribute("id"); //id = "liczbamin|wsp col|wsp row"
  y = parseInt(id.substring(3,5));
  x = parseInt(id.substring(1,3))

  if (kwadraty[x][y].getAttribute("class") == "kwadracik"){
    kwadraty[x][y].setAttribute("class", "kwadracik flaga");
    zostalomin--;
    document.getElementById('licznikmin').innerHTML = zostalomin;
    if (zostalomin == 0)
      sprawdzWarunki();
  }
  else if (kwadraty[x][y].getAttribute("class") == "kwadracik flaga") {
    kwadraty[x][y].setAttribute("class", "kwadracik");
    clearTimeout(lklik);
    zostalomin++;
    document.getElementById('licznikmin').innerHTML = zostalomin;
  }
}

function liczbaMin(m,x,y){  //przeszukuje sasiednie pola w poszukiwaniu min i zwraca ich liczbe
  liczba = 0;
  var i,maxi,maxj,j;
  if (x>0)
    i = x-1;
  else
    i = x;
  if (x<15)
    maxi = x+1;
  else
    maxi = x;
  if (y<15)
    maxj = y+1;
  else
    maxj = y;

  for (; i<= maxi; i++){
    if (y>0)
      j = y-1;
    else
      j = y;
    for (; j<= maxj; j++){
      if (m[i][j]==-1){
        liczba++;
      }
    }
  }
  return liczba;
}

function sprawdzWarunki(){    //sprawdza czy sa spelnione warunki do ukonczenia gry
  if (odsPola==216 && zostalomin==0){
    koniec = true;
    var node = document.createElement("DIV");
    var textnode = document.createTextNode("Gratulacje! Pomyślne ukończenie gry!");
    node.appendChild(textnode);
    document.getElementById("main").appendChild(node);
  }
}

function startTime() {    //aktualizuje zegar z czasem gry
  if (koniec)
    return;
  var teraz = new Date();
  var roznica = new Date(teraz-t0);
  var m = roznica.getMinutes();
  var s = roznica.getSeconds();
  var time;
  document.getElementById('zegar').innerHTML = m + ":" + s;
  var t = setTimeout(startTime, 500);
}
